package util

class ResponseMatchers {

    private static isList(def body) {
        return body instanceof List<Map>
    }

    static assertAllCommentsContainTheSamePostId(def comments, int postId) {
        if (isList(comments)) assert (comments as List<Map>).every { it.postId == postId }
        else assert (comments as Map).postId == postId
        return true
    }

    static assertAllPostsContainTheSameUserId(def posts, int userId) {
        if (isList(posts)) assert (posts as List<Map>).every { it.userId == userId }
        else assert (posts as Map).userId == userId
        return true
    }

    static assertPostsStructureIsValid(def posts) {
        if (isList(posts)) posts.every { checkPostStructure(it) }
        else checkPostStructure(posts as Map)
        return true
    }

    private static checkPostStructure(def post) {
        assert post.userId instanceof Number
        assert post.id instanceof Number
        assert post.body instanceof String
        assert post.title instanceof String &&
            post.title != ''
        return true
    }

    static assertCommentsStructureIsValid(def comments) {
        if (isList(comments)) comments.every { checkCommentStructure(it) }
        else checkCommentStructure(comments as Map)
        return true
    }

    private static checkCommentStructure(def comment) {
        assert comment.postId instanceof Number
        assert comment.id instanceof Number
        assert comment.body instanceof String &&
            comment.body != ''
        assert comment.name instanceof String &&
            comment.name != ''
        assert comment.email instanceof String &&
            comment.email != ''
        return true
    }
}
