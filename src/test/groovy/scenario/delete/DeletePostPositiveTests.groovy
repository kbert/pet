package scenario.delete

import org.apache.http.HttpStatus
import scenario.AbstractTest
import spock.lang.Ignore

import static com.mashape.unirest.http.Unirest.delete

class DeletePostPositiveTests extends AbstractTest {

    @Ignore('I would argue with HTTP 200 instead of 204 in this case because of empty response-body. Decided not to report though')
    def 'test 1: when deleting a post -> should receive empty response-body and HTTP 204'() {

        given: 'the system under test has post with id: 1'
                def postId = 1

        when: 'user deletes post with id: 1'
                def response = delete("$baseUrl/posts/$postId").asJson()

        then: 'user receives empty response-body and HTTP 204'
                assert response.status == HttpStatus.SC_NO_CONTENT

                def responseBody = parser.parse(response.rawBody)
                assert responseBody == Collections.emptyMap()
    }
}
