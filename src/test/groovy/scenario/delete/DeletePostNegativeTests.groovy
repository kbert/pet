package scenario.delete

import org.apache.http.HttpStatus
import scenario.AbstractTest
import spock.lang.Ignore

import static com.mashape.unirest.http.Unirest.delete

class DeletePostNegativeTests extends AbstractTest {

    @Ignore('Not sure if bulk-delete is supposed to be supported, so decided to ask the guys: https://github.com/typicode/jsonplaceholder/issues/110')
    def 'test 1: when deleting several posts in one request -> should receive HTTP 400'() {

        given: 'the system under test has posts with id: 1 and id: 2'
                def postId1 = 1
                def postId2 = 2

        when: 'user deletes both these posts in one request'
                def response = delete("$baseUrl/posts/$postId1,$postId2").asJson()

        then: 'user receives HTTP 400'
                assert response.status == HttpStatus.SC_BAD_REQUEST
                /*And most probably some error-message*/
    }

    @Ignore('Returns HTTP 200. Reported by some other guys: https://github.com/typicode/jsonplaceholder/issues/93')
    def 'test 2: when deleting non-existing post -> should receive empty response-body and HTTP 404'() {

        given: 'the system under test does not have a post with id: 1000'
                def nonExistingPostId = 1000

        when: 'user deletes non-existing post'
                def response = delete("$baseUrl/posts/$nonExistingPostId").asJson()

        then: 'user receives HTTP 404'
                assert response.status == HttpStatus.SC_NOT_FOUND

                def responseBody = parser.parse(response.rawBody)
                assert responseBody == Collections.emptyMap()
    }
}
