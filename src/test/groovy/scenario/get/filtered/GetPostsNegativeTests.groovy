package scenario.get.filtered

import org.apache.http.HttpStatus
import scenario.AbstractTest
import spock.lang.Ignore

import static com.mashape.unirest.http.Unirest.get

class GetPostsNegativeTests extends AbstractTest {

    def 'test 1: when fetching posts, specifying invalid resource-identifier -> should receive empty response-body and HTTP 404'() {

        given: 'fall-through'

        when: 'user misspells resource-identifier'
                def response = get("$baseUrl/postssssss").asJson()

        then: 'user receives empty response-body and HTTP 404'
                assert response.status == HttpStatus.SC_NOT_FOUND

                def responseBody = parser.parse(response.rawBody)
                assert responseBody == Collections.emptyMap()
    }

    @Ignore('HTTP 200 instead of 404. At this time reported by me (yeah!): https://github.com/typicode/jsonplaceholder/issues/109')
    def 'test 2: when fetching all posts of non-existing user -> should receive empty response-body and HTTP 404'() {

        given: 'the system under test does not have posts created by user with id: 1000'
                def nonExistingUserId = 1000

        when: 'user fetches post, specifying userId=1000'
                def response = get("$baseUrl/posts?userId=$nonExistingUserId").asJson()

        then: 'user receives empty response-body and HTTP 404'
                assert response.status == HttpStatus.SC_NOT_FOUND

                def responseBody = parser.parse(response.rawBody)
                assert responseBody == Collections.emptyMap()
    }
}
