package scenario.get.filtered

import org.apache.http.HttpStatus
import scenario.AbstractTest
import spock.lang.Issue

import static com.mashape.unirest.http.Unirest.get
import static util.ResponseMatchers.*

class GetPostsPositiveTests extends AbstractTest {

    def 'test 1: when fetching all posts -> should receive all posts and HTTP 200'() {

        given: 'the system under test has 100 posts'
                def postsCount = 100

        when: 'user fetches all post'
                def response = get("$baseUrl/posts").asJson()

        then: 'user receives all 100 posts and HTTP 200'
                assert response.status == HttpStatus.SC_OK

                List<Map> posts = parser.parse(response.rawBody) as List<Map>
                assert posts.size() == postsCount
                assertPostsStructureIsValid(posts)
    }

    def 'test 2: when fetching all posts of specific user -> should receive all posts, created by specific user and HTTP 200'() {

        given: 'the system under test has 10 posts created by user with id: 1'
                def userId = 1
                def postsCount = 10

        when: 'user fetches posts created by user with id: 1'
                def response = get("$baseUrl/posts?userId=$userId").asJson()

        then: 'user receives 10 posts and HTTP 200'
                assert response.status == HttpStatus.SC_OK

                List<Map> posts = parser.parse(response.rawBody) as List<Map>
                assert posts.size() == postsCount
                assertAllPostsContainTheSameUserId(posts, userId)
                assertPostsStructureIsValid(posts)
    }
}
