package scenario.get.id

import org.apache.http.HttpStatus
import scenario.AbstractTest
import spock.lang.Ignore

import static com.mashape.unirest.http.Unirest.get
import static util.ResponseMatchers.*

class GetPostByIdPositiveTests extends AbstractTest {

    def 'test 1: when fetching post by id -> should receive 1 post with requested id and HTTP 200'() {

        given: 'the system under test has post with id: 100'
                def postId = 100

        when: 'user fetches post with id: 100'
                def response = get("$baseUrl/posts/$postId").asJson()

        then: 'user receives post with id: 100 and HTTP 200'
                assert response.status == HttpStatus.SC_OK

                Map post = parser.parse(response.rawBody) as Map
                assertPostsStructureIsValid(post)
                assert post.id == postId
    }

    @Ignore("Returns more than should. Open issue here: https://github.com/typicode/jsonplaceholder/issues/106")
    def 'test 2: when fetching comments of specific post -> should receive comments associated with requested post and HTTP 200'() {

        given: 'the system under test has comments under post with id: 100'
                def postId = 100

        when: 'user fetches comments of post with id: 100'
                def response = get("$baseUrl/posts/$postId/comments").asJson()

        then: 'user receives comments associated with post with id: 100 and HTTP 200'
                assert response.status == HttpStatus.SC_OK

                def comments = parser.parse(response.rawBody)
                assertAllCommentsContainTheSamePostId(comments, postId)
                assertCommentsStructureIsValid(comments)
    }
}
