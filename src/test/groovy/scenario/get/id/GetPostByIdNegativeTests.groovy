package scenario.get.id

import org.apache.http.HttpStatus
import scenario.AbstractTest
import spock.lang.Unroll

import static com.mashape.unirest.http.Unirest.get

@Unroll
class GetPostByIdNegativeTests extends AbstractTest {

    def 'test 1: when fetching non-existing post -> should receive empty response-body and HTTP 404'() {

        given: 'the system under test does not have a post with id: 1000'
                def nonExistingPostId = 1000

        when: 'user fetches post with id: 1000'
                def response = get("$baseUrl/posts/$nonExistingPostId").asJson()

        then: 'user receives empty response-body and HTTP 404'
                assert response.status == HttpStatus.SC_NOT_FOUND

                def responseBody = parser.parse(response.rawBody)
                assert responseBody == Collections.emptyMap()
    }

    def 'test 2: when fetching post by invalid id (#invalidId) -> should receive empty response-body and HTTP 404'() {

        given: 'fall-through'

        when: 'user fetches a post by invalid id'
                def response = get("$baseUrl/posts/$invalidId").asJson()

        then: 'user receives empty response-body and HTTP 404'
                assert response.status == HttpStatus.SC_NOT_FOUND

                def responseBody = parser.parse(response.rawBody)
                assert responseBody == Collections.emptyMap()

        where:
                invalidId << [
                    'one',
                    'id-1',
                    1.5,
                    -4,
                ]
    }
}
