package scenario

import groovy.json.JsonSlurper
import spock.lang.*

class AbstractTest extends Specification {

    static JsonSlurper parser = new JsonSlurper()
    static String baseUrl = 'https://jsonplaceholder.typicode.com'

    def setupSpec() {
        /*Setup env here: instantiate mocks, establish connections, etc..
          This section is run once for all tests, pretty much like @BeforeAll in JUnit*/
    }

    def setup() {
        /*Setup something specific for each test, e.g. configure default mock-response if needed*/
    }

    def cleanup() {
        /*Cleanup after each test, e.g. erase db contents created by a test, reset mocks configuration*/
    }

    def cleanupSpec() {
        /*Here kill all connections, etc*/
    }
}
